package gestaohotel;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class MenuBase {

	protected abstract Menu montaMenu();

	protected abstract void retornoMenu(MenuOpcao menuOpcao);

	public void mostraCabecalho(String titulo) {
		Messages.show("***********************");
		Messages.show(titulo);
		Messages.show("***********************");
	}

	private MenuOpcao mostraMenu() {
		Menu menu = montaMenu();
		mostraCabecalho("Escolha uma opção:");
		menu.renderizar();
		Scanner in = new Scanner(System.in);
		MenuOpcao menuOpcao = null;
		try {
			Integer codigo = in.nextInt();
			menuOpcao = menu.porCodigo(codigo);
		} catch (InputMismatchException e) {
		}
		retornoMenu(menuOpcao);
		return menuOpcao;
	}

	public MenuOpcao inicia() {
		return mostraMenu();
	}

}
