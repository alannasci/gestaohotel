package gestaohotel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FormCheckOut extends FormBase<Checkout> {

	private Checkout checkout = new Checkout();
	private List<Quarto> quartoLista;

	public FormCheckOut(List<Quarto> quartoLista) {
		super();
		this.quartoLista = quartoLista;
	}

	public List<Quarto> getQuartoLista() {
		return quartoLista;
	}

	@Override
	public Checkout inicia() {
		Scanner in = new Scanner(System.in);
		Checkout checkout = new Checkout();
		MenuQuarto menuQuarto = new MenuQuartoOcupados();
		menuQuarto.setQuartoList(quartoLista);
		MenuOpcao opcaoQuarto = menuQuarto.inicia();
		Quarto quartoEscolhido = menuQuarto.getQuartoDaOpcao(opcaoQuarto);
		if(quartoEscolhido == null) {
			return null;
		}
		checkout.fazerCheckout(quartoEscolhido, quartoEscolhido.getHospede());
		Messages.show("Quantidade de noites: ");
		checkout.setNoites(in.nextInt());
		Messages.show("Forma de pagamento: ");
		checkout.setFormaPagamento(in.nextLine());
		Messages.show(checkout);
		return checkout;
	}
}
