package gestaohotel;

public class MenuOpcao {
	private Integer codigo;
	private String label;

	public MenuOpcao(Integer codigo, String label) {
		this.label = label;
		this.codigo = codigo;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "MenuOpcao [label=" + label + ", codigo=" + codigo + "]";
	}

}
