package gestaohotel;

public class Constants {
	public static final int MENU_CADASTRO_CLIENTE = 1;
	public static final int MENU_CADASTRO_QUARTO = 2;
	public static final int MENU_CHECKOUT = 3;
	public static final int MENU_CANCELAR = 0;
	
	public static final int TIPO_VIAGEM_FERIAS = 1;
	public static final int TIPO_VIAGEM_TRABALHO = 2;
	public static final int TIPO_VIAGEM_EVENTOS = 3;
	
}
