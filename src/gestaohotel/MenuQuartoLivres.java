package gestaohotel;

import java.util.ArrayList;
import java.util.List;

public class MenuQuartoLivres extends MenuQuarto {
	@Override
	public void setQuartoList(List<Quarto> quartoLista) {
		List<Quarto> quartoListaOcupados = new ArrayList<Quarto>();
		for (Quarto quarto : quartoLista) {
			if (quarto.estaLivre()) {
				quartoListaOcupados.add(quarto);
			}
		}
		super.setQuartoList(quartoListaOcupados);
	}
}
