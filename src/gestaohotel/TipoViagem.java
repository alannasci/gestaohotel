package gestaohotel;

public class TipoViagem {

	private String title;
	private Integer id;

	public TipoViagem(String title, Integer id) {
		super();
		this.title = title;
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TipoViagem [title=" + title + ", id=" + id + "]";
	}

}
