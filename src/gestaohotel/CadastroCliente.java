package gestaohotel;

public class CadastroCliente {
	private String nome;
	private String sobrenome;
	private String dataNascimento;
	private String email;
	private String celular;
	private String enderecoOrigem;
	private TipoViagem tipoViagem;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobreNome) {
		this.sobrenome = sobreNome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEnderecoOrigem() {
		return enderecoOrigem;
	}

	public void setEnderecoOrigem(String enderecoOrigem) {
		this.enderecoOrigem = enderecoOrigem;
	}

	public TipoViagem getTipoViagem() {
		return tipoViagem;
	}

	public void setTipoViagem(TipoViagem tipoViagem) {
		this.tipoViagem = tipoViagem;
	}

	@Override
	public String toString() {
		return "CadastroCliente [nome=" + nome + ", sobreNome=" + sobrenome + ", dataNascimento=" + dataNascimento
				+ ", email=" + email + ", celular=" + celular + ", enderecoOrigem=" + enderecoOrigem + ", tipoViagem="
				+ tipoViagem + "]";
	}

}
