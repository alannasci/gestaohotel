package gestaohotel;

import java.util.Scanner;

public class FormCadastroCliente extends FormBase<CadastroCliente> {

	private CadastroCliente cliente = new CadastroCliente();

	@Override
	public CadastroCliente inicia() {
		Scanner in = new Scanner(System.in);
		Messages.show("Nome: ");
		cliente.setNome(in.nextLine());

		Messages.show("Sobrenome: ");
		cliente.setSobrenome(in.nextLine());

		Messages.show("Data de Nascimento (DD/MM/YYYY): ");
		cliente.setDataNascimento(in.nextLine());

		Messages.show("E-mail: ");
		cliente.setEmail(in.nextLine());

		Messages.show("Celular: ");
		cliente.setCelular(in.nextLine());

		Messages.show("Endereço de origem: ");
		cliente.setEnderecoOrigem(in.nextLine());

		MenuTipoViagem menuTipoViagem = new MenuTipoViagem();
		menuTipoViagem.mostraCabecalho("Escolha o tipo de viagem: ");
		MenuOpcao menuOpcao = menuTipoViagem.inicia();
		cliente.setTipoViagem(new TipoViagem(menuOpcao.getLabel(), menuOpcao.getCodigo()));

		Messages.show(cliente);
		
		return cliente;
	}
}
