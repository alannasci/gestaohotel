package gestaohotel;

import java.math.BigDecimal;

public class Quarto {
	private Integer numero;
	private BigDecimal valorNoite;
	private CadastroCliente hospede;

	public CadastroCliente getHospede() {
		return hospede;
	}

	public void setHospede(CadastroCliente hospede) {
		this.hospede = hospede;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public BigDecimal getValorNoite() {
		return valorNoite;
	}

	public void setValorNoite(BigDecimal valorNoite) {
		this.valorNoite = valorNoite;
	}

	public Boolean estaLivre() {
		return hospede == null;
	}

	@Override
	public String toString() {
		return "Quarto [numero=" + numero + ", valorNoite=" + valorNoite + ", hospede=" + hospede + "]";
	}

}
