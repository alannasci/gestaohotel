package gestaohotel;

import java.math.BigDecimal;

public class Checkout {
	private Quarto quarto;
	private CadastroCliente cliente;
	private Integer noites;
	private String formaPagamento;

	public BigDecimal getTotal() {
		return quarto.getValorNoite().multiply(new BigDecimal(noites));
	}

	public Integer getNoites() {
		return noites;
	}

	public void setNoites(Integer noites) {
		this.noites = noites;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public CadastroCliente getCliente() {
		return cliente;
	}

	public void fazerCheckout(Quarto quarto, CadastroCliente cliente) {
		this.quarto = quarto;
		quarto.setHospede(null);
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "Checkout [quarto=" + quarto + ", cliente=" + cliente + ", noites=" + noites + ", formaPagamento="
				+ formaPagamento + "]";
	}

}
