package gestaohotel;

import java.util.ArrayList;
import java.util.List;

public class MenuQuartoOcupados extends MenuQuarto {

	@Override
	public void setQuartoList(List<Quarto> quartoLista) {
		List<Quarto> quartoListaLivres = new ArrayList<Quarto>();
		for (Quarto quarto : quartoLista) {
			if (!quarto.estaLivre()) {
				quartoListaLivres.add(quarto);
			}
		}
		super.setQuartoList(quartoListaLivres);
	}

}
