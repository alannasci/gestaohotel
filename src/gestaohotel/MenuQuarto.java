package gestaohotel;

import java.util.List;

public class MenuQuarto extends MenuBase {
	private List<Quarto> quartoLista;

	public void setQuartoList(List<Quarto> quartoLista) {
		this.quartoLista = quartoLista;
	}

	@Override
	protected Menu montaMenu() {
		Menu menu = new Menu();
		mostraCabecalho("ESCOLHA UM QUARTO: ");
		for (Quarto quarto : quartoLista) {
			menu.adicionarOpcao(new MenuOpcao(quarto.getNumero(), "Quarto " + quarto.getNumero().toString()));
		}
		return menu;
	}

	public Quarto getQuartoDaOpcao(MenuOpcao menuOpcao) {
		for (Quarto quarto : quartoLista) {
			if (quarto.getNumero().equals(menuOpcao.getCodigo())) {
				return quarto;
			}
		}
		return null;
	}

	@Override
	public void retornoMenu(MenuOpcao menuOpcao) {
	}

}
