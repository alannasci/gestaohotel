package gestaohotel;

import java.util.ArrayList;
import java.util.List;

public class InterfaceUsuario extends MenuBase {

	List<CadastroCliente> clienteLista = new ArrayList<CadastroCliente>();
	List<Quarto> quartoLista = new ArrayList<Quarto>();

	@Override
	protected Menu montaMenu() {
		Menu menu = new Menu();
		menu.adicionarOpcao(new MenuOpcao(Constants.MENU_CADASTRO_CLIENTE, "Cadastro de cliente"));
		menu.adicionarOpcao(new MenuOpcao(Constants.MENU_CADASTRO_QUARTO, "Cadastro de quarto"));
		menu.adicionarOpcao(new MenuOpcao(Constants.MENU_CHECKOUT, "Realizar Check-Out"));
		return menu;
	}

	@Override
	public void retornoMenu(MenuOpcao menuOpcao) {
		if(menuOpcao == null) {
			inicia();
			return;
		}
		switch (menuOpcao.getCodigo()) {
		case Constants.MENU_CADASTRO_CLIENTE:
			if (quartoLista.isEmpty()) {
				Messages.alert("Erro: Primeiro cadastre pelo menos um quarto");
				inicia();
				return;
			}
			FormCadastroCliente formCadastroCliente = new FormCadastroCliente();
			CadastroCliente cadastroCliente = formCadastroCliente.inicia();
			aoCadastrarCliente(cadastroCliente);
			MenuQuarto menuQuarto = new MenuQuartoLivres();
			menuQuarto.setQuartoList(quartoLista);
			MenuOpcao opcaoQuarto = menuQuarto.inicia();
			Quarto quartoEscolhido = menuQuarto.getQuartoDaOpcao(opcaoQuarto);
			quartoEscolhido.setHospede(cadastroCliente);
			Messages.show(
					"Cliente " + cadastroCliente.getNome() + " fez check-in no quarto " + quartoEscolhido.getNumero());
			inicia();
			break;
		case Constants.MENU_CHECKOUT:
			FormCheckOut formCheckOut = new FormCheckOut(quartoLista);
			Checkout checkout = formCheckOut.inicia();
			Messages.show("Checkout do cliente "+ checkout.getCliente().getNome() +" do quarto "+ checkout.getQuarto().getNumero() +" realizado com sucesso!");
			Messages.show("Total: R$ " + checkout.getTotal());
			Messages.show("Pagamento: " + checkout.getFormaPagamento());
			inicia();
			break;
		case Constants.MENU_CADASTRO_QUARTO:
			FormCadastroQuarto formCadastroQuarto = new FormCadastroQuarto();
			Quarto quarto = formCadastroQuarto.inicia();
			aoCadastrarQuarto(quarto);
			inicia();
			break;
		case Constants.MENU_CANCELAR:
			Messages.show("Finalizado.");
			break;
		default:
			break;
		}
	}

	private void aoCadastrarCliente(CadastroCliente cadastroCliente) {
		if (cadastroCliente == null) {
			Messages.show("ERRO AO CADASTRAR CLIENTE!!");
			return;
		}
		clienteLista.add(cadastroCliente);
		Messages.show("CLIENTE CADASTRADO COM SUCESSO!!");
	}

	private void aoCadastrarQuarto(Quarto quarto) {
		if (quarto == null) {
			Messages.show("ERRO AO CADASTRAR QUARTO!!");
			return;
		}
		quartoLista.add(quarto);
		Messages.show("QUARTO CADASTRADO COM SUCESSO!!");
	}

	private Quarto pegarQuartoPorNumero(Integer numero) {
		for (Quarto quarto : quartoLista) {
			if (quarto.getNumero().equals(numero)) {
				return quarto;
			}
		}
		return null;
	}

}
