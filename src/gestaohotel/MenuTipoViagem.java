package gestaohotel;

public class MenuTipoViagem extends MenuBase {

	@Override
	protected Menu montaMenu() {
		Menu menu = new Menu();
		menu.adicionarOpcao(new MenuOpcao(Constants.TIPO_VIAGEM_FERIAS, "Ferias"));
		menu.adicionarOpcao(new MenuOpcao(Constants.TIPO_VIAGEM_TRABALHO, "Trabalho"));
		menu.adicionarOpcao(new MenuOpcao(Constants.TIPO_VIAGEM_EVENTOS, "Evento"));
		return menu;
	}

	@Override
	public void retornoMenu(MenuOpcao menuOpcao) {
	}

}
