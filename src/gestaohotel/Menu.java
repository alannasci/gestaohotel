package gestaohotel;

import java.util.ArrayList;
import java.util.List;

public class Menu implements UiComponente {
	private List<MenuOpcao> opcoes = new ArrayList<MenuOpcao>();

	void adicionarOpcao(MenuOpcao menuOpcao) {
		if (temOpcao(menuOpcao)) {
			return;
		}
		opcoes.add(menuOpcao);
	}

	private Boolean temOpcao(MenuOpcao menuOpcaoChecar) {
		MenuOpcao opcao = porCodigo(menuOpcaoChecar.getCodigo());
		return opcao != null;
	}
	
	public MenuOpcao porCodigo(Integer codigo) {
		for (MenuOpcao menuOpcao : opcoes) {
			if (menuOpcao.getCodigo().equals(codigo)) {
				return menuOpcao;
			}
		}
		return null;
	}

	public List<MenuOpcao> getMenuList() {
		return opcoes;
	}

	@Override
	public void renderizar() {
		for (MenuOpcao menuOpcao : opcoes) {
			Messages.show("[ " + menuOpcao.getCodigo() + " ] = " + menuOpcao.getLabel());
		}
		Messages.show("[ sair ] = inicio");
	}

	public interface RetornoMenu {
		void escolheuOpcao(MenuOpcao menuOpcao);
	}
}
