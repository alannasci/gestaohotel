package gestaohotel;

import java.math.BigDecimal;
import java.util.Scanner;

public class FormCadastroQuarto extends FormBase<Quarto> {

	private Quarto quarto = new Quarto();

	@Override
	public Quarto inicia() {
		Scanner in = new Scanner(System.in);
		
		Messages.show("Numero do quarto: ");
		quarto.setNumero(in.nextInt());

		Messages.show("Valor da noite: ");
		quarto.setValorNoite(new BigDecimal(in.nextFloat()));
		
		Messages.show(quarto);
		
		return quarto;
	}
}
